import time

# These are the simulation of the goUp and goDown hardware functions.
# everyone of them receives the initial floor and return the +-1 floor if the operation is successfuly done
# otherwise it return the same position. If there is any harware error stoping the mechanism the hardware returns an error code
# error code means means the hardware is stuck and you need to call the technical service.
# After integrating the real hardware functions, we need to adapt the input/output.

def goUp(current):
  time.sleep(1.5)
  print("UP to "+ str(current+1)+ " ... ")
  time.sleep(0.5)
  return current+1
  
def goDown(current):
  time.sleep(1.5)
  print("Down to "+ str(current-1)+ " ... ")
  time.sleep(0.5)  
  return current - 1

# Time sleep is simulating hardware fonctionalities