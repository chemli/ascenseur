'''
                                            Elevator Software By CHEMLI Mohamed Ali 
                        version : 1.00
                        contact : chemli@eurecom.fr

'''
import hardwareAPI as hw
from flask import Flask, request, render_template
import requests,time
import threading

#  Configuration. Normally you need to move it to a seperate config.ini file and pick up data from there
FLOOR_MAX = 10
FLOOR_MIN = -2
INITIAL_FLOOR_NUMBER = -1
ERROR_CODE = 10000

# Initialization
Floor = {"currentFloorNumber" : INITIAL_FLOOR_NUMBER ,  "floorMax" : FLOOR_MAX  , "floorMin" : FLOOR_MIN  , "Tasks":[]}
app = Flask(__name__)


# move function receives the destination and doesnt return anything 
def move(dest):
    # if we are in the target floor, do nothing and delete the task
    if dest== Floor["currentFloorNumber"]: 
        Floor["Tasks"].remove(Floor["currentFloorNumber"])
        print("Floor "+ str(Floor["currentFloorNumber"])+ " served")
    # if we have to go up, we call goUp() until reaching the target. at every floor we check if there is a task calling it, if yes we stop and continue.
    if dest> Floor["currentFloorNumber"]:
        while dest != Floor["currentFloorNumber"]:
            Floor["currentFloorNumber"] = hw.goUp(Floor["currentFloorNumber"])
            if Floor["currentFloorNumber"] in Floor["Tasks"]:
                  Floor["Tasks"].remove(Floor["currentFloorNumber"])
                  time.sleep(1)
                  print("Floor "+ str(Floor["currentFloorNumber"])+ " served")
            # If the hardware returning Error code, we stop everything and call for help      
            elif  Floor["currentFloorNumber"] == ERROR_CODE:
                print(" Calling for help. Hardware is stuck")
                Floor["Tasks"] = []
                Floor["currentFloorNumber"] = INITIAL_FLOOR_NUMBER
                return
    # if we have to go down, we call goDown() until reaching the target. at every floor we check if there is a task calling it, if yes we stop and continue.
    if dest< Floor["currentFloorNumber"]:
        while dest != Floor["currentFloorNumber"]:
            Floor["currentFloorNumber"] = hw.goDown(Floor["currentFloorNumber"])
            if Floor["currentFloorNumber"] in Floor["Tasks"]:
                  Floor["Tasks"].remove(Floor["currentFloorNumber"])
                  print("Floor "+ str(Floor["currentFloorNumber"])+ " served")
            # If the hardware returning Error code, we stop everything and call for help      
            elif  Floor["currentFloorNumber"] == ERROR_CODE:
                print(" Calling for help. Hardware is stuck")
                Floor["Tasks"] = []
                Floor["currentFloorNumber"] = INITIAL_FLOOR_NUMBER
                return                 

# This is an endpoint root, when called it returns the current floor. Usually it is used for the graphic controller.
@app.route('/', methods=["GET"])
def get():
    return  "current floor is "+ str(Floor["currentFloorNumber"]) + "\n"
# This function gets the target floor from the graphic controller. Clicking on floor 5 sends a request to this endpoint with argument 5.
@app.route('/<destination>', methods=["GET"])
def getrequest(destination):
    if int(destination) <= Floor["floorMax"] and int(destination) >= Floor["floorMin"] :
        Floor["Tasks"].append(int(destination))
    return str(Floor["Tasks"]) + "\n"

# This is the listener. It listens to new tasks to do. If the Tasks list is empty, The system is idle. 
# you can uncomment the time.sleep(0.2) in order to low sytems performance but win some processing time 
def listen():    
    while True:
        # time.sleep(0.2)
        if len(Floor["Tasks"]):
            move(Floor["Tasks"][0])
            #Floor["Tasks"] = Floor["Tasks"][1:]
            print(Floor["Tasks"]) 

if __name__ == '__main__': 
    # send listening thread
    threading.Thread(target=listen).start()
    print("Welcome to yout elevator software monitoring:\nHighest floor is : "+ str(FLOOR_MAX) + '\n'+"Highest floor is : "+ str(FLOOR_MIN) + "\n"+ "You are in floor : "+ str(INITIAL_FLOOR_NUMBER) )
    # listen to the requests on the main thread
    app.run(debug=True, port=8080)

    
    





  

    
    

