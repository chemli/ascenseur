# Ascenseur

# Overview 
This is the small simulation of elevator's software linked with only Up and Down Hardware functions.

## Requirements
Python 3 

## Usage
To run the server, please execute the following from the root directory.

Run the following commands :
```
pip3 install -r requirements.txt
python3 graphic.py
```
You can change the initial settings from the first lines of graphic.py file.

Elevator Controller generally contains a small screen showing current floor and buttons to requests a floor number.

To see the current floor send a request to

```
http://localhost:8080/

```

To request a floor number please send a requets containing the floor number.
To request 4th floor:

```
http://localhost:8080/4

```